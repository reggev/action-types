const should = require('chai').should();
const expect = require('chai').expect;
const actionTypes = require('./../index');

describe('test creating actions', () => {
  const syncActions = ['TOGGLE_MODAL', 'SET_NOTIFICATION', 'REMOVE_NOTIFICATION'];
  const asyncActions = ['FETCH_USER', 'POST_MESSAGE'];

  it('should return an empty object', () => {
    const res = actionTypes();
    res.should.be.empty
  });

  it('only sync', () => {
    const res = actionTypes({ syncActions });
    expect(res).to.deep.equal({
      TOGGLE_MODAL: 'TOGGLE_MODAL',
      SET_NOTIFICATION: 'SET_NOTIFICATION',
      REMOVE_NOTIFICATION: 'REMOVE_NOTIFICATION'
    });
  });

  it('only async', () => {
    const res = actionTypes({ asyncActions });
    expect(res).to.deep.equal({
      FETCH_USER: 'FETCH_USER',
      FETCH_USER_PENDING: 'FETCH_USER_PENDING',
      FETCH_USER_SUCCESS: 'FETCH_USER_SUCCESS',
      FETCH_USER_FAIL: 'FETCH_USER_FAIL',

      POST_MESSAGE: 'POST_MESSAGE',
      POST_MESSAGE_PENDING: 'POST_MESSAGE_PENDING',
      POST_MESSAGE_SUCCESS: 'POST_MESSAGE_SUCCESS',
      POST_MESSAGE_FAIL: 'POST_MESSAGE_FAIL'
    });
  });

  it('both sync and async', () => {
    const res = actionTypes({ asyncActions, syncActions });
    expect(res).to.deep.equal({
      FETCH_USER: 'FETCH_USER',
      FETCH_USER_PENDING: 'FETCH_USER_PENDING',
      FETCH_USER_SUCCESS: 'FETCH_USER_SUCCESS',
      FETCH_USER_FAIL: 'FETCH_USER_FAIL',

      POST_MESSAGE: 'POST_MESSAGE',
      POST_MESSAGE_PENDING: 'POST_MESSAGE_PENDING',
      POST_MESSAGE_SUCCESS: 'POST_MESSAGE_SUCCESS',
      POST_MESSAGE_FAIL: 'POST_MESSAGE_FAIL',

      TOGGLE_MODAL: 'TOGGLE_MODAL',
      SET_NOTIFICATION: 'SET_NOTIFICATION',
      REMOVE_NOTIFICATION: 'REMOVE_NOTIFICATION'
    });
  });


});