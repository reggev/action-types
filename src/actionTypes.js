const setupSyncActions = (actions = []) => actions.reduce((acc, type) => Object.assign({}, acc, { [type]: type }), {});
const setupAsyncActions = (actions = []) => actions.reduce((acc, type) => Object.assign({}, acc,
  {
    [type]: type,
    [`${type}_SUCCESS`]: `${type}_SUCCESS`,
    [`${type}_PENDING`]: `${type}_PENDING`,
    [`${type}_FAIL`]: `${type}_FAIL`
  }
), {});

const actions = ({ asyncActions, syncActions } = {}) =>
  Object.assign({}, setupAsyncActions(asyncActions), setupSyncActions(syncActions));

module.exports = actions;