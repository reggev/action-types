const actionTypes = require('./../src/actionTypes');

module.exports = actionTypes({
  syncActions: ['TOGGLE_MODAL'],
  asyncActions: [
    'FETCH_USER',
    'FETCH_FRIENDS'
  ]
});
