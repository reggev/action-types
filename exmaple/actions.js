const types = require('./types');

const toggleModal = () => ({
  type: types.TOGGLE_MODAL
});

const fetchUser = (userId) => (dispatch, getState) => {
  dispatch({ type: types.FETCH_USER_PENDING });
  fetch(`http://myApi/users/${userId}`)
    .then(user => dispatch({ type: types.FETCH_USER_SUCCSS, payload: { user } }))
    .catch(err => dispatch({ type: types.FETCH_USER_FAIL, error: true, payload: { err } }));
};

module.exports = {
  toggleModal,
  fetchUser
};