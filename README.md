# action-types #
### A simple utility for creating unified action types for redux action creators ###

Items in the sync actions array generate a single entry: 
```
{MY_ACTION: 'MY_ACTION'}
```
Items in the async actions array will generate 4 entries:
```
{
  MY_ACTION: 'MY_ACTION'
  MY_ACTION_PENDING: 'MY_ACTION_PENDING'
  MY_ACTION_SUCCESS: 'MY_ACTION_SUCCESS'
  MY_ACTION_FAIL: 'MY_ACTION_FAIL'
}

```
### Example: ###
File: types.js

```
import actions from 'action-types';

export default actions({
  syncActions: ['TOGGLE_MODAL'],
  asyncActions: ['FETCH_USER', 'FETCH_FRIENDS']
});
```

will export the object:
```
{
  TOGGLE_MODAL: 'TOGGLE_MODAL',
  FETCH_USER: 'FETCH_USER',
  FETCH_USER_PENDING: 'FETCH_USER_PENDING',
  FETCH_USER_SUCCESS: 'FETCH_USER_SUCCESS',
  FETCH_USER_FAIL: 'FETCH_USER_FAIL',
  FETCH_FRIENDS: 'FETCH_FRIENDS',
  FETCH_FRIENDS_PENDING: 'FETCH_FRIENDS_PENDING',
  FETCH_FRIENDS_SUCCESS: 'FETCH_FRIENDS_SUCCESS',
  FETCH_FRIENDS_FAIL: 'FETCH_FRIENDS_FAIL'
}
```

File: someActionCreator.js

```
import types from './types.js';

// sync action creator
export const toggleModal = () => ({
  type: types.TOGGLE_MODAL
});

// a thunk
export const fetchUser = (userId) => (dispatch, getState) => {
  dispatch({type: types.FETCH_USER_PENDING});
  fetch('http://myApi/user/:userId')
  .then(user => dispatch({
    type: types.FETCH_USER_SUCCESS,
    payload: {user}
  }))
  .catch(err => dispatch({
    type: FETCH_USER_FAIL,
    error: true,
    payload: {err}
  }));
}
```